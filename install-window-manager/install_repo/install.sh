#!/bin/bash

#==============================================
# install.sh --- install script for doomnvim
# Author: Pierre-Yves Douault
# License: MIT
#==============================================

# Code shamelessly stolen from SpaceVim installer
# check https://spacevim.org for more info

# Init option --------------------------------------------------{{{
Color_off='\033[0m'       # Text Reset

# terminal color template --------------------------------------{{{
# Regular Colors
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# Bold
BBlack='\033[1;30m'       # Black
BRed='\033[1;31m'         # Red
BGreen='\033[1;32m'       # Green
BYellow='\033[1;33m'      # Yellow
BBlue='\033[1;34m'        # Blue
BPurple='\033[1;35m'      # Purple
BCyan='\033[1;36m'        # Cyan
BWhite='\033[1;37m'       # White

# }}}



msg() {
    printf '%b\n' "$1" >&2
}

success() {
    msg "${Green}[✔]${Color_off} ${1}${2}"
}

info() {
    msg "${Blue}[+]${Color_off} ${1}${2}"
}

error() {
    msg "${Red}[X]${Color_off} ${1}${2}"
    exit 1
}

warn () {
    msg "${Yellow}[!]${Color_off} ${1}${2}"
}

echo_with_color () {
    printf '%b\n' "$1$2$Color_off" >&2
}

check_config(){
    mkdir -p $HOME/.config
    mkdir -p $HOME/.scripts
}

install_packages(){
    info "Installing packages"
    sudo pacman -S bspwm sxhkd rofi dunst 
}

create_dirs(){
    info "Populating required directories"
    cp -r bspwm/ $HOME/.config/bspwm
    cp -r dunst/ $HOME/.config/dunst
    cp -r picom $HOME/.config/picom
    cp -r polybar $HOME/.config/polybar
    cp -r rofi $HOME/.config/rofi
    cp -r sxhkd $HOME/.config/sxhkd
}

config_xprofile(){
    echo "export XDG_CURRENT_DESKTOP=KDE" >> $HOME/.xprofile
    echo "export XDG_SESION_DESKTOP=KDE" >> $HOME/.xprofile
    echo "export SAL_USE_VCLPLUGIN=kde5" >> $HOME/.xprofile
    echo "export KDE_SESSION_VERSION=5">> $HOME/.xprofile
}


main(){
    check_config 
    install_packages
    create_dirs
    config_xprofile
}

main
