git clone https://aur.archlinux.org/yay-bin.git
cd yay-bin/
makepkg -si
cd install_repo
chmod +x install.sh
bash install.sh

systemctl start vboxservice
cp -r install_repo/scripts/* /home/harold/.scripts

yay -S ranger
yay -S feh
yay -S deadd-notification-center-bin
yay -S nerd-fonts-hasklig

echo -e "Attention, il faut éventuellement modifier le fichier /home/harold/.config/polybar/launch.sh avec le bon nom d'écran (lancer la commande xrandr pour savoir) \n"
echo -e "Attention, il faut éventuellement modifier le fichier /home/harold/.config/bspwm/bspwmrc avec le bon nom d'écran (lancer la commande xrandr pour savoir) \n"
