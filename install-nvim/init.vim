"PACKER CONFIGURATION
call plug#begin()

Plug 'jvirtanen/vim-hcl'


call plug#end()


set mouse=a
set clipboard=unnamed
set clipboard+=unnamedplus
set laststatus=2

set termguicolors

set nu					
:syntax on			
:filetype on		
set smartindent	
set autoindent

colorscheme doomnvim

" FORCE REMAP
nnoremap <MiddleMouse> "+p<CR> 


