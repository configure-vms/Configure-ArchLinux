#!/bin/bash

echo "vim installation ...\n"
sudo pacman -Syy vim --noconfirm
echo "git installation ... \n"
sudo pacman -S git --noconfirm
echo "pull git configuration repository ... \n"
mkdir /home/$USER/git-repo-configure-ArchLinux
cd /home/$USER/git-repo-configure-ArchLinux
echo "python3 installation ... \n"
sudo pacman -S python3 --noconfirm
echo "pip-python installation ... \n"
sudo pacman -S pip-python --no-confirm|
echo "ansible installation... \n"
pip3 install ansible
echo "Copie de l'installation de ansible dans /usr/bin"
if [[ -d /home/$USER/.local/bin/ ]]
then
	cd /home/$USER/.local/bin && sudo cp * /usr/bin
	echo "SUCCESS : ansible copy in /usr/bin"
else
	echo "WARNING : No ansible installation in /home/$USER/.local/bin/"
fi
echo "openssh installation ...\n"
sudo pacman -S openssh --noconfirm

